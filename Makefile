TARGET = main
LIB = therms_lib.a

CXX = mpicc
LD = mpicc
AR = ar

CXX_FLAGS = -Wall -Wextra 
LD_FLAGS =
AR_FLAGS = rs

OBJ_DIR = bin
SRC_DIR = src

MAIN_SRCS = main.c
LIB_SRCS =  client.c \
			signal.c \
			logger.c \
			def.c \
			communication.c \
			changing_room.c \
			shower.c \
			changing_room_communication.c \
			shower_communication.c

MAIN_OBJS = $(addprefix $(OBJ_DIR)/,$(notdir $(MAIN_SRCS:.c=.o)))
LIB_OBJS = $(addprefix $(OBJ_DIR)/,$(notdir $(LIB_SRCS:.c=.o)))

INCLUDES = $(wildcard $(SRC_DIR)/*.h)

USER_MESSAGE = "\033[32mProgram compiled correctly!\033[m"

.PHONY: all clean

all: $(OBJ_DIR) $(OBJ_DIR)/$(TARGET)
	@echo $(USER_MESSAGE)

$(OBJ_DIR)/$(TARGET): $(MAIN_OBJS) $(OBJ_DIR)/$(LIB)
	$(LD) $(LD_FLAGS) -o $(OBJ_DIR)/$(TARGET) $^

$(OBJ_DIR)/$(LIB): $(LIB_OBJS)
	$(AR) $(AR_FLAGS) $(OBJ_DIR)/$(LIB) $^

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c $(INCLUDES)
	$(CXX) $(CXX_FLAGS) -o $@ -c $<

$(OBJ_DIR):
	mkdir -p $(OBJ_DIR)

clean:
	-rm -rf $(OBJ_DIR)/*.o
	-rm -rf $(OBJ_DIR)/*.a
	-rm -rf $(OBJ_DIR)/$(TARGET)
