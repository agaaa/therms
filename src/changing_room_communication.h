#ifndef CHANGING_ROOM_COMMUNICATION
#define CHANGING_ROOM_COMMUNICATION

#include <stdio.h>

#include "def.h"
#include "communication.h"

void enter_changing_room_req_msg_handle(int _my_id, int _msg_from, int _gender,
        int *_my_changing_room_time_id, int *_changing_room_time_id, int *tab_not_empty, int *tab_not);

void enter_changing_room_resp_msg_handle(int _msg_from, int *_full_resp, int *_not_empty_resp);

void enter_changing_room_msg_handle(int _msg_from, int *_number_changing_room, int *_gender_changing_room);

void left_changing_room_msg_handle(int _msg_from, int *_number_changing_room, int *_gender_changing_room);

void block_changing_room_msg_handle(int _msg_from, int *_lock_changing_room);

void unblock_changing_room_msg_handle(int _msg_from, int *_lock_changing_room);

#endif // CHANGING_ROOM_COMMUNICATION
