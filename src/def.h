#ifndef DEF
#define DEF

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "logger.h"

#define M 2 //number of cabinets at the pool
#define P 3 //number of showers

//index in tables
#define ENTER_CH_ROOM_REQ 0
#define ENTER_CH_ROOM_RESP 1
#define ENTER_CH_ROOM 2
#define LEFT_CH_ROOM 3
#define ENTER_SHOWER_REQ 4
#define ENTER_SHOWER_RESP 5
#define ENTER_SHOWER 6
#define LEFT_SHOWER 7
#define BLOCK_CH_ROOM 8
#define BLOCK_SHOWER 9
#define UNBLOCK_CH_ROOM 10
#define UNBLOCK_SHOWER 11

#define FULL_AGREEMENT 0
#define NOT_EMPTY_AGREEMENT 1
#define NOT_EMPTY_TO_FULL_AGREEMENT 2

const char* concatenate(const char* substr1, const char* substr2);
const char* int_to_str(int num);
const char* tab_to_str(int *_tab, int tab_size);
void MPI_error_message(const char *MPI_function, int _num, int _id);
int randomTime(int minTime, int maxTime);
int get_opposite_gender(int _gender);

#endif // DEF
