#include "shower_communication.h"

void enter_shower_req_msg_handle(int _my_id, int _msg_from, int _my_gender,
        int *_my_shower_time_id, int *_shower_time_id, int *tab_not_empty, int *tab_not) {

    ++*(_shower_time_id);

    int received_shower_time_id = msg[ENTER_SHOWER_REQ][_msg_from][0];
    int received_shower_gender = msg[ENTER_SHOWER_REQ][_msg_from][1];
    int buf[msg_size[ENTER_SHOWER_RESP]];

    // FULL AGREEMENT
    if( (*_my_shower_time_id == -1)
            || (*_my_shower_time_id > received_shower_time_id)
            || (*_my_shower_time_id == received_shower_time_id && _my_id > _msg_from) ) {
        buf[0] = FULL_AGREEMENT;
        send_msg(_my_id, _msg_from, ENTER_SHOWER_RESP, buf);
    }
    else if(received_shower_gender !=  _my_gender) { // NOT EMPTY AGREEMENT
        buf[0] = NOT_EMPTY_AGREEMENT;
        send_msg(_my_id, _msg_from, ENTER_SHOWER_RESP, buf);
        tab_not_empty[_msg_from] = 1;
    } else {
        tab_not[_msg_from] = 1;
    }

}

// ------------------------------------------------------- //

void enter_shower_resp_msg_handle(int _msg_from, int *_full_resp, int *_not_empty_resp) {
    switch(msg[ENTER_SHOWER_RESP][_msg_from][0]) {
        case FULL_AGREEMENT:
            *_full_resp = *_full_resp + 1;  
            break;
        case NOT_EMPTY_AGREEMENT:
            *_not_empty_resp = *_not_empty_resp + 1;
            break;
        case NOT_EMPTY_TO_FULL_AGREEMENT:
            *_full_resp = *_full_resp + 1;  
            *_not_empty_resp = *_not_empty_resp - 1;
            break;
    }   
}

// ------------------------------------------------------- //

void enter_shower_msg_handle(int _my_id, int _msg_from, int _gender, int _size, int *_number_shower, int *_gender_shower, int *_lock_shower,
        int *_shower_limit, int *_shower_enter_while_waiting, int *_full_resp, int *_not_empty_resp, int _my_shower_time_id) {
   
    *_number_shower = *_number_shower + 1;

    int recipient_gender = msg[ENTER_SHOWER][_msg_from][0];
    if (*_gender_shower == 2)
        *_gender_shower = recipient_gender;

    if(_my_shower_time_id != -1) {
        if(recipient_gender != _gender)
            *_shower_enter_while_waiting = *_shower_enter_while_waiting + 1;
        else 
            *_shower_enter_while_waiting = 0;
    }

    if((*_shower_enter_while_waiting > *_shower_limit)  && (*_full_resp + *_not_empty_resp == _size - 1)) {
        int opposite_gender = get_opposite_gender(_gender); //recipient ok
       *_lock_shower = opposite_gender;

       int buf[msg_size[BLOCK_SHOWER]];
       buf[0] = opposite_gender;
       send_msg_to_all(_my_id, _size, BLOCK_SHOWER, buf);

    }
}

// ------------------------------------------------------- //

void left_shower_msg_handle(int *_number_shower, int *_gender_shower) {
    *_number_shower = *_number_shower - 1;
    if(*_number_shower == 0)
        *_gender_shower = 2;
}

// ------------------------------------------------------- //

void block_shower_msg_handle(int _msg_from, int *_lock_shower) {
    int gender_to_block = msg[BLOCK_SHOWER][_msg_from][0];
    *_lock_shower = gender_to_block;
}

// ------------------------------------------------------- //

void unblock_shower_msg_handle(int *_lock_shower) {
    *_lock_shower = 2;
}


