#include "changing_room_communication.h"

void enter_changing_room_req_msg_handle(int _my_id, int _msg_from, int _my_gender,
        int *_my_changing_room_time_id, int *_changing_room_time_id, int *tab_not_empty, int *tab_not) {

    ++(*_changing_room_time_id);

    int received_ch_room_time_id = msg[ENTER_CH_ROOM_REQ][_msg_from][0];
    int received_gender = msg[ENTER_CH_ROOM_REQ][_msg_from][1];
    int buf[msg_size[ENTER_CH_ROOM_RESP]];

    // FULL AGREEMENT
    if( (*_my_changing_room_time_id == -1)
            || (*_my_changing_room_time_id > received_ch_room_time_id)
            || (*_my_changing_room_time_id == received_ch_room_time_id && _my_id > _msg_from) ) {
        buf[0] = FULL_AGREEMENT;
        send_msg(_my_id, _msg_from, ENTER_CH_ROOM_RESP, buf);
    }
    else if(received_gender !=  _my_gender) { // NOT EMPTY AGREEMENT
        buf[0] = NOT_EMPTY_AGREEMENT;
        send_msg(_my_id, _msg_from, ENTER_CH_ROOM_RESP, buf);
        tab_not_empty[_msg_from] = 1;
    } else {
        tab_not[_msg_from] = 1;
    }
}

// ------------------------------------------------------- //

void enter_changing_room_resp_msg_handle(int _msg_from, int *_full_resp, int *_not_empty_resp) {
    switch(msg[ENTER_CH_ROOM_RESP][_msg_from][0]) {
        case FULL_AGREEMENT:
            *_full_resp = *_full_resp + 1; 
            break;
        case NOT_EMPTY_AGREEMENT:
            *_not_empty_resp = *_not_empty_resp + 1;
            break;
        case NOT_EMPTY_TO_FULL_AGREEMENT:
            *_full_resp = *_full_resp + 1; 
            *_not_empty_resp = *_not_empty_resp - 1;
            break;
    }
}

// ------------------------------------------------------- //

void enter_changing_room_msg_handle(int _msg_from, int *_number_changing_room, int *_gender_changing_room) {
    int changing_room_id = msg[ENTER_CH_ROOM][_msg_from][0];
    int changing_room_gender = msg[ENTER_CH_ROOM][_msg_from][1];

    *(_number_changing_room + changing_room_id) = *(_number_changing_room + changing_room_id) + 1;

    if(*(_gender_changing_room + changing_room_id) == 2)
        *(_gender_changing_room + changing_room_id) = changing_room_gender;
}

// ------------------------------------------------------- //

void left_changing_room_msg_handle(int _msg_from, int *_number_changing_room, int *_gender_changing_room) {
    int changing_room_id = msg[LEFT_CH_ROOM][_msg_from][0];
    *(_number_changing_room + changing_room_id) = *(_number_changing_room + changing_room_id) - 1;
    if(*(_number_changing_room + changing_room_id) == 0)
        *(_gender_changing_room + changing_room_id) = 2;
}

// ------------------------------------------------------- //

void block_changing_room_msg_handle(int _msg_from, int *_lock_changing_room) {
    int changing_room_id = msg[BLOCK_SHOWER][_msg_from][0];
    int gender_to_block = msg[BLOCK_SHOWER][_msg_from][1];
    *(_lock_changing_room + changing_room_id) = gender_to_block;
}

// ------------------------------------------------------- //

void unblock_changing_room_msg_handle(int _msg_from, int *_lock_changing_room) {
    int changing_room_id = msg[UNBLOCK_CH_ROOM][_msg_from][0];
    *(_lock_changing_room + changing_room_id) = 2;
}

