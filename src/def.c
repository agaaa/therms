#include "def.h"

const char* concatenate(const char* substr1, const char* substr2) {

    char* str;
    str = (char*) malloc(strlen(substr1)+strlen(substr2)+1); 
    strcpy(str, substr1); 
    strcat(str, substr2);

    return str;
}

// ------------------------------------------------------- //

const char* int_to_str(int _num) {
    char num[10] = {'\0'};
    snprintf(num, sizeof(num), "%d", _num);

    unsigned int i=0;
    while((i < 10) && (num[i] != '\0')) {
        ++i;
    }

    char* str = (char*) malloc(i);
    strcpy(str, num); 
    return str;
}

// ------------------------------------------------------- //

const char* tab_to_str(int *_tab, int tab_size) {
    int i;
    char** res;
    res = (char**) malloc(tab_size);
    for(i=0; i<tab_size; ++i) {
        char * temp = (char*) int_to_str(_tab[i]);
        if(i != tab_size-1) {
            res[i] = (char*)malloc(sizeof(temp) + 2);
            res[i] = (char*) concatenate(temp, ", ");
        }
        else {
            res[i] = (char*) malloc(sizeof(temp));
            res[i] = (char*) temp;
        }
    }
    char *result = (char*) malloc(sizeof(res) + 2);
    result = "{";
    for(i=0; i<tab_size; ++i) {
        result = (char*) concatenate(result, res[i]);
    }
        result = (char*) concatenate(result, "}");
    return result;
}

// ------------------------------------------------------- //

void MPI_error_message(const char *_MPI_function, int _num, int _id) {
    const char* msg1 = "Function \"";
    const char* msg2 = "\" returned error status: ";
    const char* num = int_to_str(_num);
    const char* message = concatenate(concatenate(concatenate(msg1, _MPI_function), msg2), num);

    add_to_logs(ERROR, _id, message);
}

// ------------------------------------------------------- //

int randomTime(int minTime, int maxTime) {
    return minTime + (rand() % (maxTime - minTime));
}

// ------------------------------------------------------- //

int get_opposite_gender(int _gender) {
    return (1+_gender) % 2;
}


