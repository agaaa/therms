#ifndef COMMUNICATION
#define COMMUNICATION

#include <stdio.h>
#include <mpi.h>

#include "logger.h"
#include "def.h"
#include "shower_communication.h"
#include "changing_room_communication.h"

#define REQUESTS_NUM 12

int my_id;
int size;
int my_gender;
int *my_changing_room;

int *my_changing_room_time_id;
int *my_shower_time_id;
int *changing_room_time_id;
int *shower_time_id;

int *table_not_empty;
int *table_not;

int *full_resp;
int *not_empty_resp;

int *number_changing_room;
int *number_shower;
int *gender_changing_room;
int *gender_shower;

int *shower_limit;
int *shower_enter_while_waiting;
int *lock_shower;
int *lock_changing_room;

MPI_Request **requests; //type, id
int ***msg; //type, id
int *msg_size;
int *tags;

// trying receive messages from all clients each type
void try_communication();

// allocating memory and initialize variables
void init_communication(int _id, int _size, int _gender, int *_MY_CHANGING_ROOM,
        int *_MY_CHANGING_ROOM_TIME_ID, int *_MY_SHOWER_TIME_ID, int *_CHANGING_ROOM_TIME_ID, int *_SHOWER_TIME_ID,
        int *_NOT_EMPTY_AGREEMENT_TABLE, int *_NO_AGREEMENT_TABLE, int *_FULL_RESPONSE_NUM, int *_NOT_EMPTY_RESPONSE_NUM,
        int *_NUMBER_CHANGING_ROOM, int *_NUMBER_SHOWER, int *_GENDER_CHANGING_ROOM, int *_GENDER_SHOWER,
        int *_SHOWER_LIMIT, int *SHOWER_ENTER_WHILE_WAITING, int *_LOCK_SHOWER, int *_LOCK_CHANGING_ROOM);
void init_msg_sizes();
void init_msg_tags();
void allocate_memory_to_messages();
void init_requests();

// send
void send_msg(int _id, int _recipient, int _tag, int *_msg);
void send_msg_to_all(int _id, int _size, int _tag, int *_msg);

MPI_Request* get_request(int _tag, int _id);

// receive
void unblocking_receive(int _id, int _tag);
void unblocking_receive_from_all(int _tag);
void unblocking_receive_all_tags();

// testing if receive is complete
void test_unblocking_receive(int _id, int _tag);
void test_unblocking_receive_from_all(int _tag);
void test_unblocking_receive_all_tags();

// appropiate function dependent on tag of received message
void msg_handle(int _type, int _id);

#endif // COMMUNICATION
