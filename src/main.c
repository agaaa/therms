#include <mpi.h>
#include <stdio.h>

#include "client.h"
#include "def.h"
#include "logger.h"


int main (int argc, char* argv[]) {
    int rank, size;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank); 
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    client_run(rank, size); 

    MPI_Finalize();
    add_to_logs(INFO, rank, "MPI finalized.");

    return 0;
}
