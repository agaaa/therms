#include "communication.h"

void try_communication() {
    test_unblocking_receive_all_tags();
}

// ------------------------------------------------------- //

void init_communication(int _id, int _size, int _gender, int *_MY_CHANGING_ROOM,
        int *_MY_CHANGING_ROOM_TIME_ID, int *_MY_SHOWER_TIME_ID, int *_CHANGING_ROOM_TIME_ID, int *_SHOWER_TIME_ID,
        int *_NOT_EMPTY_AGREEMENT_TABLE, int *_NO_AGREEMENT_TABLE, int *_FULL_RESPONSE_NUM, int *_NOT_EMPTY_RESPONSE_NUM,
        int *_NUMBER_CHANGING_ROOM, int *_NUMBER_SHOWER, int *_GENDER_CHANGING_ROOM, int *_GENDER_SHOWER,
        int *_SHOWER_LIMIT, int *_SHOWER_ENTER_WHILE_WAITING, int *_LOCK_SHOWER, int *_LOCK_CHANGIING_ROOM) {
    my_id = _id;
    size = _size;
    my_gender = _gender;
    my_changing_room = _MY_CHANGING_ROOM;
    changing_room_time_id = _CHANGING_ROOM_TIME_ID;
    shower_time_id = _SHOWER_TIME_ID;
    my_changing_room_time_id = _MY_CHANGING_ROOM_TIME_ID;
    my_shower_time_id = _MY_SHOWER_TIME_ID;
    table_not_empty = _NOT_EMPTY_AGREEMENT_TABLE;
    table_not = _NO_AGREEMENT_TABLE;
    full_resp = _FULL_RESPONSE_NUM;
    not_empty_resp = _NOT_EMPTY_RESPONSE_NUM;
    number_changing_room = _NUMBER_CHANGING_ROOM;
    number_shower = _NUMBER_SHOWER;
    gender_changing_room = _GENDER_CHANGING_ROOM;
    gender_shower = _GENDER_SHOWER;
    shower_limit = _SHOWER_LIMIT;
    shower_enter_while_waiting = _SHOWER_ENTER_WHILE_WAITING;
    lock_shower = _LOCK_SHOWER;
    lock_changing_room = _LOCK_CHANGIING_ROOM;

    init_msg_sizes();    
    init_msg_tags();
    allocate_memory_to_messages(_size);
    init_requests(_id, _size);
}

// ------------------------------------------------------- //

// allocate memory and initialize sizes of massages each type
void init_msg_sizes() {
    msg_size = (int*) malloc(REQUESTS_NUM * sizeof(int));
    msg_size[ENTER_CH_ROOM_REQ] = 2; // MY_CHANGING_ROOM_TIME_ID, gender
    msg_size[ENTER_CH_ROOM_RESP] = 1; // a kind of AGREEMENT
    msg_size[ENTER_CH_ROOM] = 2; // changing room id, gender
    msg_size[LEFT_CH_ROOM] = 1; // changing room id 
    msg_size[ENTER_SHOWER_REQ] = 2; // MY_SHOWER_TIME_ID, gender  
    msg_size[ENTER_SHOWER_RESP] = 1; // a kind of AGREEMENT
    msg_size[ENTER_SHOWER] = 1; // gender 
    msg_size[LEFT_SHOWER] = 1; // -
    msg_size[BLOCK_CH_ROOM] = 2; // changing room id, gender to block 
    msg_size[BLOCK_SHOWER] = 1;  // gender to block
    msg_size[UNBLOCK_CH_ROOM] = 1; // changing room id
    msg_size[UNBLOCK_SHOWER] = 1; // -
}

// ------------------------------------------------------- //

// allocate memory and initialize tags of massages each type (it can be any other numbers)
void init_msg_tags() {
    tags = (int*) malloc(REQUESTS_NUM * sizeof(int));
    int i;
    for(i=0; i<REQUESTS_NUM; ++i) 
        tags[i] = 111*(i+1); 
}

// ------------------------------------------------------- //

// allocate memory to receiving messages
void allocate_memory_to_messages() {
    msg = (int***) malloc(REQUESTS_NUM * sizeof(int**));
    int i, j;    
    for(i=0; i<REQUESTS_NUM; ++i) {
        msg[i] = (int**) malloc(size * sizeof(int*));
        for(j=0; j<size; ++j) {
            msg[i][j] = (int*) malloc(msg_size[i] * sizeof(int));
        }
    }
}

// ------------------------------------------------------- //

// allocate memory to requests (connected with receiving messages)
void init_requests() {
    requests = (MPI_Request**) malloc(REQUESTS_NUM * sizeof(MPI_Request*)); //number of tags
    int i;    
    for(i=0; i<REQUESTS_NUM; ++i) 
        requests[i] = (MPI_Request*) malloc(size * sizeof(MPI_Request));
    unblocking_receive_all_tags(my_id, size);
}

// ------------------------------------------------------- //

// blocking send (one message)
void send_msg(int _id, int _recipient, int _type, int *_msg) {
    int result = MPI_Send(_msg, msg_size[_type], MPI_INT, _recipient, tags[_type], MPI_COMM_WORLD);
    if(result != MPI_SUCCESS) 
        MPI_error_message("MPI_Isend", result, _id);
}

// ------------------------------------------------------- //

// blocking send (messages to all clients)
void send_msg_to_all(int _id, int _size, int _type, int *_msg) {
    int i;
    for (i=0; i<_size; i++) {
        if (i == _id) 
            continue;
        send_msg(_id, i, _type, _msg);
    }
}

// ------------------------------------------------------- //

MPI_Request* get_request(int _type, int _num) {
    return &requests[_type][_num];
}

// ------------------------------------------------------- //

// unblocking receive (one message)
void unblocking_receive(int _id, int _type) {
    MPI_Request *request = get_request(_type, _id);
    int result = MPI_Irecv(msg[_type][_id], msg_size[_type], MPI_INT, _id, tags[_type], MPI_COMM_WORLD, request);
    if(result != MPI_SUCCESS) 
        MPI_error_message("MPI_Irecv", result, _id);
}

// ------------------------------------------------------- //

// unblocking receive from all clients (one type)
void unblocking_receive_from_all(int _type) {
    int i;
    for(i=0; i<size; ++i) {
        if(i == my_id)
            continue;
        unblocking_receive(i, _type);
    }
}

// ------------------------------------------------------- //

// unblocking receive from all clients (all types)
void unblocking_receive_all_tags() {
    int i;
    for(i=0; i<REQUESTS_NUM; ++i) 
        unblocking_receive_from_all(i);
}

// ------------------------------------------------------- //

// testing if irecv call is completed
void test_unblocking_receive(int _id, int _type) {
    MPI_Status status;
    MPI_Request *request = get_request(_type, _id);
    int flag;
    int result = MPI_Test(request, &flag, &status);
    if(result != MPI_SUCCESS) 
        MPI_error_message("MPI_Test", result, _id);
    if(flag > 0) {
        msg_handle(_type, _id);
        unblocking_receive(_id, _type);
    }
}

// ------------------------------------------------------- //

// unblocking receive from all clients (one type)
void test_unblocking_receive_from_all(int _type) {
    int i;
    for(i=0; i<size; ++i) {
        if(i == my_id)
            continue;
        test_unblocking_receive(i, _type);
    }
}

// ------------------------------------------------------- //

// unblocking receive from all clients (all types)
void test_unblocking_receive_all_tags() {
    int i;
    for(i=0; i<REQUESTS_NUM; ++i) 
        test_unblocking_receive_from_all(i);
}

// ------------------------------------------------------- //

void msg_handle(int _type, int _id) {
    switch (_type) {
        case ENTER_CH_ROOM_REQ :
            enter_changing_room_req_msg_handle(my_id, _id, my_gender,
                    my_changing_room_time_id, changing_room_time_id, table_not_empty, table_not);
            break;
        case ENTER_CH_ROOM_RESP :
            enter_changing_room_resp_msg_handle(_id, full_resp, not_empty_resp);
            break;
        case ENTER_CH_ROOM  :
            enter_changing_room_msg_handle(_id, number_changing_room, gender_changing_room); 
            break;
        case LEFT_CH_ROOM :
            left_changing_room_msg_handle(_id, number_changing_room, gender_changing_room);
            break;
        case ENTER_SHOWER_REQ :
            enter_shower_req_msg_handle(my_id, _id, my_gender,
                    my_shower_time_id, shower_time_id, table_not_empty, table_not);
            break;
        case ENTER_SHOWER_RESP :
            enter_shower_resp_msg_handle(_id, full_resp, not_empty_resp);
            break;
        case ENTER_SHOWER :
            enter_shower_msg_handle(my_id, _id, my_gender, size, number_shower, gender_shower, lock_shower,
                    shower_limit, shower_enter_while_waiting, full_resp, not_empty_resp, *my_shower_time_id);
            break;
        case LEFT_SHOWER :
            left_shower_msg_handle(number_shower, gender_shower);
            break;
        case BLOCK_CH_ROOM :
            block_changing_room_msg_handle(_id, lock_changing_room);
            break;
        case BLOCK_SHOWER :
            block_shower_msg_handle(_id, lock_shower);
            break;
        case UNBLOCK_CH_ROOM : 
            unblock_changing_room_msg_handle(_id, lock_changing_room);
            break;
        case UNBLOCK_SHOWER :
            unblock_shower_msg_handle(lock_shower);
            break;
    }
}

