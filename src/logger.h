#ifndef LOGGER
#define LOGGER

#include <stdio.h>
#include <time.h>
#include <string.h>

#define ENTER_SECTION 0
#define CRITICAL_SECTION 1
#define EXIT_SECTION 2
#define INFO 3
#define ERROR 4

#define ENTER_COLOR  "\e[1;34m" //blue
#define CRITICAL_COLOR "\e[1;32m" //green
#define EXIT_COLOR "\e[1;35m" //magenta
#define INFO_COLOR  "\e[1;33m" //yellow
#define ERROR_COLOR "\e[1;31m" //red
#define DEFAULT_COLOR "\e[1;37m" //white
#define COLOR_END "\e[0m"

void add_to_logs(int log_type, int id, const char *message);

#endif // LOGGER
