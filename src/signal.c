#include "signal.h"

void signal_handler(int _sig_num) {
    
    const char* msg = "Caught signal ";
    const char* num = int_to_str(_sig_num);
    const char* message = concatenate(msg, num);

    add_to_logs(INFO, id, message);
    stop = 1;
}

// ------------------------------------------------------- //

void set_signal_hadling(int _id) {
    id = _id;
    stop = 0;
    
    if (signal(SIGUSR1, signal_handler) == SIG_ERR) {
        add_to_logs(ERROR, id, "An error occurred while setting a signal handler.\n");
    }
}

// ------------------------------------------------------- //

int stopRunning() {
    return stop;
}
