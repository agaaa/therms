#include "client.h"

void client_run(int _id, int _size) {
    start_up(_id, _size);
    main_loop();
}

// ------------------------------------------------------- //

void start_up(int _id, int _size) {
    id = _id;
    size = _size;
    set_random_gender();

    set_signal_hadling(id);
    init_variables();
    init_communication(id, size, gender, &MY_CHANGING_ROOM_NUM,
            &MY_CHANGING_ROOM_TIME_ID, &MY_SHOWER_TIME_ID, &CHANGING_ROOM_TIME_ID, &SHOWER_TIME_ID, 
            NOT_EMPTY_AGREEMENT_TABLE, NO_AGREEMENT_TABLE, &FULL_RESPONSE_NUM, &NOT_EMPTY_RESPONSE_NUM,
            NUMBER_CHANGING_ROOM, &NUMBER_SHOWER, GENDER_CHANGING_ROOM, &GENDER_SHOWER, 
            &SHOWER_TIME_ID, &SHOWER_ENTER_WHILE_WAITING, &LOCK_SHOWER, LOCK_CHANGING_ROOM); 
    state = &enter_changing_room_init;
}

// ------------------------------------------------------- //

void set_random_gender() {
    srand(((id+1)*time(NULL))+id);
    gender = rand()%2; 

    const char *msg_man = "Client process started (gender: man)";
    const char *msg_woman = "Client process started (gender: woman)";
    
    if(gender == 0)    
        add_to_logs(INFO, id, msg_man);
    else
        add_to_logs(INFO, id, msg_woman);
}

// ------------------------------------------------------- //

void init_variables() {
    CHANGING_ROOM_TIME_ID = 0; 
    MY_CHANGING_ROOM_TIME_ID = -1; 
    SHOWER_TIME_ID = 0; 
    MY_SHOWER_TIME_ID = -1;

    MY_CHANGING_ROOM_NUM = -1;
    SHOWER_LIMIT = (int) 0.2 * size;
    SHOWER_ENTER_WHILE_WAITING = 0;

    GENDER_CHANGING_ROOM[0] = 2;
    GENDER_CHANGING_ROOM[1] = 2;
    GENDER_CHANGING_ROOM[2] = 2;
    GENDER_SHOWER = 2; 

    NUMBER_CHANGING_ROOM[0] = 0;
    NUMBER_CHANGING_ROOM[1] = 0;
    NUMBER_CHANGING_ROOM[2] = 0;
    NUMBER_SHOWER = 0; 

    LOCK_CHANGING_ROOM[0] = 2;
    LOCK_CHANGING_ROOM[1] = 2;
    LOCK_CHANGING_ROOM[2] = 2;
    LOCK_SHOWER = 2;

    NOT_EMPTY_AGREEMENT_TABLE = (int*) malloc(size * sizeof(int));
    NO_AGREEMENT_TABLE = (int*) malloc(size * sizeof(int));
    int i;
    for(i=0; i<size; ++i) {
        NOT_EMPTY_AGREEMENT_TABLE[i] = 0;
        NO_AGREEMENT_TABLE[i] = 0;
    }
}

// ------------------------------------------------------- //

void main_loop() {
    while(stopRunning() == 0) {
        try_communication();
        (*state)();
        usleep(1);
    }
}

// ------------------------------------------------------- //

void enter_changing_room_init() {
    enter_changing_room_init_procedure(id, size, gender, 
            GENDER_CHANGING_ROOM, NUMBER_CHANGING_ROOM, LOCK_CHANGING_ROOM,
            &CHANGING_ROOM_TIME_ID, &MY_CHANGING_ROOM_TIME_ID,
            &FULL_RESPONSE_NUM, &NOT_EMPTY_RESPONSE_NUM);
    add_to_logs(ENTER_SECTION, id, concatenate("Want to enter to changing room - changing room time id: ", 
                int_to_str(MY_CHANGING_ROOM_TIME_ID)));
    state = &enter_changing_room_waiting;
}

// ------------------------------------------------------- //

void enter_changing_room_waiting() {
    MY_CHANGING_ROOM_NUM = enter_changing_room_waiting_procedure(size, gender,
            &FULL_RESPONSE_NUM, &NOT_EMPTY_RESPONSE_NUM,
            GENDER_CHANGING_ROOM, NUMBER_CHANGING_ROOM, LOCK_CHANGING_ROOM);
   if(MY_CHANGING_ROOM_NUM != -1) 
        state = &enter_changing_room;
}

// ------------------------------------------------------- //

void enter_changing_room() {
    enter_changing_room_procedure(id, size, gender, MY_CHANGING_ROOM_NUM,
            &MY_CHANGING_ROOM_TIME_ID, NUMBER_CHANGING_ROOM, GENDER_CHANGING_ROOM, LOCK_CHANGING_ROOM,
            NOT_EMPTY_AGREEMENT_TABLE, NO_AGREEMENT_TABLE);
    end_time = time(NULL) + randomTime(1, 3);
    add_to_logs(CRITICAL_SECTION, id, concatenate("In changing room number ", int_to_str(MY_CHANGING_ROOM_NUM)));
    state = &stay_in_changing_room;
}

// ------------------------------------------------------- //

void stay_in_changing_room() {
    if(end_time <= time(NULL))  {
        state = &enter_shower_init; 
    }
}

// ------------------------------------------------------- //

void enter_shower_init() {
    enter_shower_init_procedure(id, size, gender,
            &SHOWER_TIME_ID, &MY_SHOWER_TIME_ID, &SHOWER_ENTER_WHILE_WAITING,
            &FULL_RESPONSE_NUM, &NOT_EMPTY_RESPONSE_NUM);
    add_to_logs(ENTER_SECTION, id, concatenate("Want to enter the shower - shower time id: ", int_to_str(MY_SHOWER_TIME_ID)));
    state = &enter_shower_waiting;
}

// ------------------------------------------------------- //

void enter_shower_waiting() {
    if(enter_shower_waiting_procedure(size, gender,
            &FULL_RESPONSE_NUM, &NOT_EMPTY_RESPONSE_NUM,
            &GENDER_SHOWER, &NUMBER_SHOWER, &LOCK_SHOWER) == 1) 
        state = &enter_shower;
}

// ------------------------------------------------------- //

void enter_shower() {
    enter_shower_procedure(id, size, gender, 
            &MY_SHOWER_TIME_ID, &GENDER_SHOWER, &NUMBER_SHOWER, &LOCK_SHOWER,
            NOT_EMPTY_AGREEMENT_TABLE, NO_AGREEMENT_TABLE, &SHOWER_ENTER_WHILE_WAITING);
    end_time = time(NULL) + randomTime(1, 2);
    add_to_logs(CRITICAL_SECTION, id, "Having a shower");
    state = &have_a_shower;
}

// ------------------------------------------------------- //

void have_a_shower() {
    if(end_time <= time(NULL)) {
        add_to_logs(EXIT_SECTION, id, "Left a shower");
        state = &left_shower;
    }
}

// ------------------------------------------------------- //

void left_shower() {
    left_shower_procedure(id, size, &GENDER_SHOWER, &NUMBER_SHOWER);
    end_time = time(NULL) + randomTime(7, 12);
    add_to_logs(INFO, id, "In swimming pool");
    state = &in_swimming_pool; 
}

// ------------------------------------------------------- //

void in_swimming_pool() {
    if(end_time <= time(NULL)) {
        end_time = time(NULL) + randomTime(1, 3);
        add_to_logs(INFO, id, "In changing room again");
        state = &in_changing_room_going_out; 
    }
}

// ------------------------------------------------------- //

void in_changing_room_going_out() {
    if(end_time <= time(NULL))  
        state = &left_changing_room; 
}

// ------------------------------------------------------- //

void left_changing_room() {
    int nu = MY_CHANGING_ROOM_NUM;
    left_changing_room_procedure(id, size, &MY_CHANGING_ROOM_NUM, NUMBER_CHANGING_ROOM, GENDER_CHANGING_ROOM);
    end_time = time(NULL) + randomTime(10, 15);
    add_to_logs(EXIT_SECTION, id, concatenate("Left changing room number ", int_to_str(nu)));
    add_to_logs(INFO, id, "Outside");
    state = &outside;
}

// ------------------------------------------------------- //

void outside() {
    if(end_time <= time(NULL))  
        state = &enter_changing_room_init;
}

