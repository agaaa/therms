#ifndef CLIENT
#define CLEINT

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <mpi.h>
#include <string.h>

#include "signal.h"
#include "logger.h"
#include "def.h"
#include "communication.h"
#include "changing_room.h"
#include "shower.h"

// number of responses to enter to changing room / shower request
int FULL_RESPONSE_NUM; 
int NOT_EMPTY_RESPONSE_NUM; 

int CHANGING_ROOM_TIME_ID; //number of received messages with tag ENTER_CHANGING_ROOM_TAG
int SHOWER_TIME_ID; //number of received messages with tag ENTER_SHOWER_TAG
int MY_CHANGING_ROOM_TIME_ID; //client number in changing room queue 
int MY_SHOWER_TIME_ID; //client number in shower queue 

// gender of people in changing room / having shower
int GENDER_CHANGING_ROOM[3];
int GENDER_SHOWER; 

// number of people in changing room / having shower
int NUMBER_CHANGING_ROOM[3];
int NUMBER_SHOWER; 

// blockade
int LOCK_CHANGING_ROOM[3];
int LOCK_SHOWER; 

int *NOT_EMPTY_AGREEMENT_TABLE;
int *NO_AGREEMENT_TABLE;

// critical line
int SHOWER_LIMIT;
int SHOWER_ENTER_WHILE_WAITING;

int id;
int size;
int gender;
int threads_num;
int MY_CHANGING_ROOM_NUM;

time_t end_time;

void (*state)();

void client_run(int _id, int _size);

// initial operations
void start_up(int _id, int _size);
void set_random_gender();
void init_variables();

void main_loop();

void enter_changing_room_init();
void enter_changing_room_waiting();
void enter_changing_room();
void stay_in_changing_room();
void enter_shower_init();
void enter_shower_waiting();
void enter_shower();
void have_a_shower();
void left_shower();
void in_swimming_pool();
void in_changing_room_going_out();
void left_changing_room();
void outside();

#endif // CLIENT
