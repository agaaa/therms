#include "logger.h"

void add_to_logs(int log_type, int id, const char *message) {

    time_t current_time = time(NULL);
    char* c_time_string = ctime(&current_time);
    c_time_string[strlen(c_time_string)-1] = '\0';
    
    switch(log_type) {
        case ENTER_SECTION:
            printf("%s %s[ENTER SECTION] [Client %d] %s %s\n", c_time_string, ENTER_COLOR, id, message, COLOR_END);
            break;
        case CRITICAL_SECTION:
            printf("%s %s[CRITICAL SECTION] [Client %d] %s %s\n", c_time_string, CRITICAL_COLOR, id, message, COLOR_END);
            break;
        case EXIT_SECTION:
            printf("%s %s[EXIT SECTION] [Client %d] %s %s\n", c_time_string, EXIT_COLOR, id, message, COLOR_END);
            break;
        case INFO:
            printf("%s %s[INFO] [Client %d] %s %s\n", c_time_string, INFO_COLOR, id, message, COLOR_END);
            break;
        case ERROR:
            printf("%s %s[ERROR][Client %d] %s %s\n", c_time_string, ERROR_COLOR, id, message, COLOR_END);
            break;
        default:
            printf("%s %s[X][Client %d] %s %s\n", c_time_string, DEFAULT_COLOR, id, message, COLOR_END);
    }
}
