#include "changing_room.h"

void enter_changing_room_init_procedure(int _id, int _size, int _gender, 
        int *_GENDER_CHANGING_ROOM, int *_NUMBER_CHANGING_ROOM, int *_LOCK_CHANGING_ROOM,
        int *_CHANGING_ROOM_TIME_ID, int *_MY_CHANGING_ROOM_TIME_ID,
        int *_FULL_RESPONSE_NUM, int *_NOT_EMPTY_RESPONSE_NUM) {
    //1a
    if(all_changing_rooms_occupied_by_opposite_gender(_gender, _GENDER_CHANGING_ROOM) == 1)
        block_changing_room(_id, _size, _gender, _NUMBER_CHANGING_ROOM, _LOCK_CHANGING_ROOM);

    //1b
    *_MY_CHANGING_ROOM_TIME_ID = *_CHANGING_ROOM_TIME_ID + 1;

    //1c
    *_FULL_RESPONSE_NUM = 0;
    *_NOT_EMPTY_RESPONSE_NUM = 0;

    //1d
    int buf[msg_size[ENTER_CH_ROOM_REQ]];
    buf[0] = *_MY_CHANGING_ROOM_TIME_ID; buf[1] = _gender;
    send_msg_to_all(_id, _size, ENTER_CH_ROOM_REQ, buf);
}

// ------------------------------------------------------- //

int enter_changing_room_waiting_procedure(int _size, int _gender,
        int *_FULL_RESPONSE_NUM, int *_NOT_EMPTY_RESPONSE_NUM,
        int *_GENDER_CHANGING_ROOM, int *_NUMBER_CHANGING_ROOM, int *_LOCK_CHANGING_ROOM) {

    int i;
    for(i=0; i<3; ++i) { //for each changing room
        if(condition_ch1(_size, _gender, _FULL_RESPONSE_NUM, _NOT_EMPTY_RESPONSE_NUM,
                    *(_GENDER_CHANGING_ROOM+i), *(_NUMBER_CHANGING_ROOM+i), *(_LOCK_CHANGING_ROOM+i) ) == 1) {
            return i;
        }   
    }

    for(i=0; i<3; ++i) { //for each changing room
        if(condition_ch2(_size, _gender, 
                    _FULL_RESPONSE_NUM, _NOT_EMPTY_RESPONSE_NUM,
                    *(_NUMBER_CHANGING_ROOM+i), *(_LOCK_CHANGING_ROOM+i) ) == 1 ) { 
            return i; 
        }
    }

    for(i=0; i<3; ++i) { //for each changing room
        if(condition_ch3(_size, _gender, _FULL_RESPONSE_NUM, *(_NUMBER_CHANGING_ROOM+i), *(_LOCK_CHANGING_ROOM+i)) == 1 ) {
            return i; 
        }
    }
    return -1;
}

// ------------------------------------------------------- //

int condition_ch1(int _size, int _gender,
        int *_FULL_RESPONSE_NUM, int *_NOT_EMPTY_RESPONSE_NUM,
        int _GENDER_CHANGING_ROOM, int _NUMBER_CHANGING_ROOM, int _LOCK_CHANGING_ROOM) {
    if( *_FULL_RESPONSE_NUM + *_NOT_EMPTY_RESPONSE_NUM == _size-1
            && _GENDER_CHANGING_ROOM == _gender
            && _NUMBER_CHANGING_ROOM < M
            && _LOCK_CHANGING_ROOM != _gender)
        return 1;
    return 0;
}

// ------------------------------------------------------- //

int condition_ch2(int _size, int _gender,
        int *_FULL_RESPONSE_NUM, int *_NOT_EMPTY_RESPONSE_NUM,
        int _NUMBER_CHANGING_ROOM, int _LOCK_CHANGING_ROOM) {
    int opposite_gender = (1+_gender) % 2;
    if (*_FULL_RESPONSE_NUM + *_NOT_EMPTY_RESPONSE_NUM == _size-1
            && _NUMBER_CHANGING_ROOM == 0
            && _LOCK_CHANGING_ROOM == opposite_gender)
        return 1;
    return 0;
}

// ------------------------------------------------------- //

int condition_ch3(int _size, int _gender, int *_FULL_RESPONSE_NUM, int _NUMBER_CHANGING_ROOM, int _LOCK_CHANGING_ROOM) {
    if (*_FULL_RESPONSE_NUM == _size-1 
            && _NUMBER_CHANGING_ROOM == 0
            && _LOCK_CHANGING_ROOM != _gender)
        return 1;
    return 0;
}

// ------------------------------------------------------- //

void enter_changing_room_procedure(int _id, int _size, int _gender, int _MY_CHANGING_ROOM_NUM,
        int *_MY_CHANGING_ROOM_TIME_ID, int *_NUMBER_CHANGING_ROOM, int *_GENDER_CHANGING_ROOM, int *_LOCK_CHANGING_ROOM,
        int *_NOT_EMPTY_AGREEMENT_TABLE, int *_NO_AGREEMENT_TABLE) {

    if(*(_GENDER_CHANGING_ROOM + _MY_CHANGING_ROOM_NUM) == 2)
        *(_GENDER_CHANGING_ROOM + _MY_CHANGING_ROOM_NUM) = _gender;

    *(_NUMBER_CHANGING_ROOM + _MY_CHANGING_ROOM_NUM) = *(_NUMBER_CHANGING_ROOM + _MY_CHANGING_ROOM_NUM) + 1;

    if(*(_LOCK_CHANGING_ROOM + _MY_CHANGING_ROOM_NUM) != 2) {
        *(_LOCK_CHANGING_ROOM + _MY_CHANGING_ROOM_NUM) = 2;

        int buf[msg_size[UNBLOCK_CH_ROOM]];
        buf[0] = _MY_CHANGING_ROOM_NUM;
        send_msg_to_all(_id, _size, UNBLOCK_CH_ROOM, buf);
    }

    int buf1[msg_size[ENTER_CH_ROOM]];
    buf1[0] = _MY_CHANGING_ROOM_NUM; buf1[1] = _gender;
    send_msg_to_all(_id, _size, ENTER_CH_ROOM, buf1);
    
    int i; 
    for(i=0; i<_size;++i) {
        if(*(_NOT_EMPTY_AGREEMENT_TABLE+i) == 1) {
            int buf[msg_size[ENTER_CH_ROOM_RESP]];
            buf[0] = NOT_EMPTY_TO_FULL_AGREEMENT;
            send_msg(_id, i, ENTER_CH_ROOM_RESP, buf);
            *(_NOT_EMPTY_AGREEMENT_TABLE+i) = 0;
        }
        if(*(_NO_AGREEMENT_TABLE+i) == 1) {
            int buf[msg_size[ENTER_CH_ROOM_RESP]];
            buf[0] = FULL_AGREEMENT;
            send_msg(_id, i, ENTER_CH_ROOM_RESP, buf);
            *(_NO_AGREEMENT_TABLE+i) = 0;
        }
    }
    
    *_MY_CHANGING_ROOM_TIME_ID = -1;

}

// ------------------------------------------------------- //

void left_changing_room_procedure(int _id, int _size, int *_my_changing_room_num,
        int *_NUMBER_CHANGING_ROOM, int *_GENDER_CHANGING_ROOM) {
    
    *(_NUMBER_CHANGING_ROOM + *_my_changing_room_num) = *(_NUMBER_CHANGING_ROOM + *_my_changing_room_num) - 1;

    if(*(_NUMBER_CHANGING_ROOM + *_my_changing_room_num) == 0)
        *(_GENDER_CHANGING_ROOM + *_my_changing_room_num) = 2;
    
    int buf[msg_size[LEFT_CH_ROOM]];
    buf[0] = *_my_changing_room_num;
    
    *_my_changing_room_num = -1;
    
    send_msg_to_all(_id, _size, LEFT_CH_ROOM, buf);
}

// ------------------------------------------------------- //

// checking if all changing rooms are occupied by apposite gender
int all_changing_rooms_occupied_by_opposite_gender(int _gender, int *GENDER_CHANGING_ROOM) {
    int opposite_gender = get_opposite_gender(_gender);
    if(GENDER_CHANGING_ROOM[0] == opposite_gender
            && GENDER_CHANGING_ROOM[1] == opposite_gender
            && GENDER_CHANGING_ROOM[2] == opposite_gender)
        return 1;
    return 0;
}

// ------------------------------------------------------- //

// lock changing room with minimum number of clients
void block_changing_room(int id, int size, int _gender, int *NUMBER_CHANGING_ROOM, int *LOCK_CHANGING_ROOM) {
    int opposite_gender = get_opposite_gender(_gender);
    int changing_room_to_block = min_changing_room(NUMBER_CHANGING_ROOM);
    LOCK_CHANGING_ROOM[changing_room_to_block] = opposite_gender; // local variable
    int buf[BLOCK_CH_ROOM];
    buf[0] = changing_room_to_block; buf[1] = opposite_gender;
    send_msg_to_all(id, size, BLOCK_CH_ROOM, buf); // message to other clients
}

// ------------------------------------------------------- //

// number of changing room with minimum number of clients
int min_changing_room(int *NUMBER_CHANGING_ROOM) {
    int min;
    if(NUMBER_CHANGING_ROOM[0] <= NUMBER_CHANGING_ROOM[1]) {
        if(NUMBER_CHANGING_ROOM[0] <= NUMBER_CHANGING_ROOM[2])
            min = 0;
        else
            min = 2;
    }
    else {
        if(NUMBER_CHANGING_ROOM[1] <= NUMBER_CHANGING_ROOM[2])
            min = 1;
        else
            min = 2;
    } 
    return min;   
}



