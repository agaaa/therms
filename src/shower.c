#include "shower.h"

void enter_shower_init_procedure(int _id, int _size, int _gender,
        int *SHOWER_TIME_ID, int *MY_SHOWER_TIME_ID, int *_SHOWER_ENTER_WHILE_WAITING,
        int *FULL_RESPONSE_NUM, int *NOT_EMPTY_RESPONSE_NUM) {

    *MY_SHOWER_TIME_ID = *SHOWER_TIME_ID + 1;

    *_SHOWER_ENTER_WHILE_WAITING = 0;

    *FULL_RESPONSE_NUM = 0;
    *NOT_EMPTY_RESPONSE_NUM = 0;

    int buf[msg_size[ENTER_SHOWER_REQ]];
    buf[0] = *MY_SHOWER_TIME_ID; buf[1] = _gender;
    send_msg_to_all(_id, _size, ENTER_SHOWER_REQ, buf);
}

// ------------------------------------------------------- //

int enter_shower_waiting_procedure(int _size, int _gender,
        int *FULL_RESPONSE_NUM, int *NOT_EMPTY_RESPONSE_NUM,
        int *GENDER_SHOWER, int *NUMBER_SHOWER, int *LOCK_SHOWER) {

    if(condition1(_size, _gender, 
                FULL_RESPONSE_NUM, NOT_EMPTY_RESPONSE_NUM,
                GENDER_SHOWER, NUMBER_SHOWER, LOCK_SHOWER) == 1
            || condition2(_size, _gender, 
                FULL_RESPONSE_NUM, 
                NUMBER_SHOWER, LOCK_SHOWER) == 1
            || condition3(_size, _gender, 
                FULL_RESPONSE_NUM, NOT_EMPTY_RESPONSE_NUM,
                NUMBER_SHOWER, LOCK_SHOWER) == 1 ) {
        return 1;
    }
    return 0;
}

// ------------------------------------------------------- //

int condition1(int _size, int _gender,
        int *FULL_RESPONSE_NUM, int *NOT_EMPTY_RESPONSE_NUM,
        int *GENDER_SHOWER, int *NUMBER_SHOWER, int *LOCK_SHOWER) {

    if( *FULL_RESPONSE_NUM + *NOT_EMPTY_RESPONSE_NUM == _size-1 
            && *GENDER_SHOWER == _gender // changing room is not empty
            && *NUMBER_SHOWER < P
            && *LOCK_SHOWER != _gender)
        return 1;
    return 0;
}

// ------------------------------------------------------- //

int condition2(int _size, int _gender,
        int *FULL_RESPONSE_NUM, int *NUMBER_SHOWER, int *LOCK_SHOWER) {
    if (*FULL_RESPONSE_NUM == _size-1 
            && *NUMBER_SHOWER == 0
            && *LOCK_SHOWER != _gender)
        return 1;
    return 0;
}

// ------------------------------------------------------- //

int condition3(int _size, int _gender,
        int *FULL_RESPONSE_NUM, int *NOT_EMPTY_RESPONSE_NUM,
        int *NUMBER_SHOWER, int *LOCK_SHOWER) {
    int opposite_gender = (1+_gender) % 2;
    if (*FULL_RESPONSE_NUM + *NOT_EMPTY_RESPONSE_NUM == _size-1 
            && *NUMBER_SHOWER == 0
            && *LOCK_SHOWER == opposite_gender)
        return 1;
    return 0;
}

// ------------------------------------------------------- //

void enter_shower_procedure(int _id, int _size, int _gender,
        int *_MY_SHOWER_TIME_ID, int *_GENDER_SHOWER, int *_NUMBER_SHOWER, int *_LOCK_SHOWER,
        int *_NOT_EMPTY_AGREEMENT_TABLE, int *_NO_AGREEMENT_TABLE, int *_ENTER_SHOWER_WHILE_WAITING) {

    if(*_GENDER_SHOWER == 2)
        *_GENDER_SHOWER = _gender;

    *_NUMBER_SHOWER = *_NUMBER_SHOWER + 1;

    *_ENTER_SHOWER_WHILE_WAITING = 0;

    if(*_LOCK_SHOWER != 2) {
        *_LOCK_SHOWER = 2;

        int buf[msg_size[UNBLOCK_SHOWER]];
        buf[0] = _id;
        send_msg_to_all(_id, _size, UNBLOCK_SHOWER, buf);
    }

    int buf[msg_size[ENTER_SHOWER]];
    buf[0] = _gender;
    send_msg_to_all(_id, _size, ENTER_SHOWER, buf);

    int i;  
    for(i=0; i<_size;++i) {
        if(*(_NOT_EMPTY_AGREEMENT_TABLE+i) == 1) {
            int buf[msg_size[ENTER_SHOWER_RESP]];
            buf[0] = NOT_EMPTY_TO_FULL_AGREEMENT;
            send_msg(_id, i, ENTER_SHOWER_RESP, buf);
            *(_NOT_EMPTY_AGREEMENT_TABLE+i) = 0;
        }
        if(*(_NO_AGREEMENT_TABLE+i) == 1) {
            int buf[msg_size[ENTER_SHOWER_RESP]];
            buf[0] = FULL_AGREEMENT;
            send_msg(_id, i, ENTER_SHOWER_RESP, buf);
            *(_NO_AGREEMENT_TABLE+i) = 0;
        }
    }

    *_MY_SHOWER_TIME_ID = -1;
}

// ------------------------------------------------------- //

void left_shower_procedure(int _id, int _size, int *_GENDER_SHOWER, int *_NUMBER_SHOWER) {

    *_NUMBER_SHOWER = *_NUMBER_SHOWER - 1;

    if(*_NUMBER_SHOWER == 0)
        *_GENDER_SHOWER = 2;

    int buf[msg_size[LEFT_SHOWER]];
    buf[0] = _id;
    send_msg_to_all(_id, _size, LEFT_SHOWER, buf);
}

