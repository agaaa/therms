#ifndef SHOWER_COMMUNICATION
#define SHOWER_COMMUNICATION

#include <stdio.h>

#include "def.h"
#include "communication.h"

void enter_shower_req_msg_handle(int _my_id, int _msg_from, int _gender,
        int *_my_shower_time_id, int *_shower_time_id, int *tab_not_empty, int *tab_not);

void enter_shower_resp_msg_handle(int _msg_from, int *_full_resp, int *_not_empty_resp);

void enter_shower_msg_handle(int _my_id, int _id_from, int _gender, int _size, int *_number_shower, int *_gender_shower, int *_lock_shower,
        int *_shower_limit, int *_shower_enter_while_waiting, int *_full_resp, int *_not_empty_resp, int _my_shower_time_id);

void left_shower_msg_handle(int *_number_shower, int *_gender_shower);

void block_shower_msg_handle(int _msg_from, int *_lock_shower);

void unblock_shower_msg_handle(int *_lock_shower);

#endif // SHOWER_COMMUNICATION
