#ifndef SHOWER
#define SHOWER

#include <stdio.h>

#include "def.h"
#include "logger.h"
#include "communication.h"

void enter_shower_init_procedure(int _id, int _size, int _gender,
        int *SHOWER_TIME_ID, int *MY_SHOWER_TIME_ID, int *_SHOWER_ENTER_WHILE_WAITING,
        int *FULL_RESPONSE_NUM, int *NOT_EMPTY_RESPONSE_NUM);

int enter_shower_waiting_procedure(int _size, int _gender,
        int *FULL_RESPONSE_NUM, int *NOT_EMPTY_RESPONSE_NUM,
        int *GENDER_SHOWER, int *NUMBER_SHOWER, int *LOCK_SHOWER);

void enter_shower_procedure(int _id, int _size, int _gender,
        int *_MY_SHOWER_TIME_ID, int *_GENDER_SHOWER, int *_NUMBER_SHOWER, int *_LOCK_SHOWER,
        int *_NOT_EMPTY_AGREEMENT_TABLE, int *_NO_AGREEMENT_TABLE, int *_ENTER_SHOWER_WHILE_WAITING);

void have_a_shower_procedure(int _id, int _size, int _gender);

void left_shower_procedure(int _id, int _size, int *_GENDER_SHOWER, int *_NUMBER_SHOWER);

int condition1(int _size, int _gender,
        int *FULL_RESPONSE_NUM, int *NOT_EMPTY_RESPONSE_NUM,
        int *GENDER_SHOWER, int *NUMBER_SHOWER, int *LOCK_SHOWER);

int condition2(int _size, int _gender,
        int *FULL_RESPONSE_NUM, int *GENDER_SHOWER, int *LOCK_SHOWER);

int condition3(int _size, int _gender,
        int *FULL_RESPONSE_NUM, int *NOT_EMPTY_RESPONSE_NUM,
        int *GENDER_SHOWER, int *LOCK_SHOWER);

#endif // SHOWER
