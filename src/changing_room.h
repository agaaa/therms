#ifndef CHANGING_ROOM
#define CHANGING_ROOM

#include <stdio.h>

#include "communication.h"
#include "def.h"

// enter functions
void enter_changing_room_init_procedure(int _id, int _size, int _gender, 
        int *GENDER_CHANGING_ROOM, int *NUMBER_CHANGING_ROOM, int *LOCK_CHANGING_ROOM, 
        int *CHANGING_ROOM_TIME_ID, int *MY_CHANGING_ROOM_TIME_ID,
        int *FULL_RESPONSE_NUM, int *NOT_EMPTY_RESPONSE_NUM);

int enter_changing_room_waiting_procedure(int _size, int _gender,
        int *FULL_RESPONSE_NUM, int *NOT_EMPTY_RESPONSE_NUM,
        int *GENDER_CHANGING_ROOM, int *NUMBER_CHANGING_ROOM, int *LOCK_CHANGING_ROOM);

int condition_ch1(int _size, int _gender,
        int *_FULL_RESPONSE_NUM, int *_NOT_EMPTY_RESPONSE_NUM,
        int _GENDER_CHANGING_ROOM, int _NUMBER_CHANGING_ROOM, int _LOCK_CHANGING_ROOM);
int condition_ch2(int _size, int _gender,
        int *_FULL_RESPONSE_NUM, int *_NOT_EMPTY_RESPONSE_NUM,
        int _GENDER_CHANGING_ROOM, int _LOCK_CHANGING_ROOM);
int condition_ch3(int _size, int _gender, int *_FULL_RESPONSE_NUM, int _GENDER_CHANGING_ROOM, int _LOCK_CHANGING_ROOM);

void enter_changing_room_procedure(int _id, int _size, int _gender, int _MY_CHANGING_ROOM_NUM,
        int *_MY_CHANGING_ROOM_TIME_ID, int *_NUMBER_CHANGING_ROOM, int *_GENDER_CHANGING_ROOM, int *_LOCK_CHANGING_ROOM,
        int *_NOT_EMPTY_AGREEMENT_TABLE, int *_NO_AGREEMENT_TABLE);

void left_changing_room_procedure(int _id, int _size, int *_my_changing_room_num,
        int *_NUMBER_CHANGING_ROOM, int *_GENDER_CHANGING_ROOM);

int all_changing_rooms_occupied_by_opposite_gender(int _gender, int *GENDER_CHANGING_ROOM);

void block_changing_room(int id, int _size, int gender, int *NUMBER_CHANGING_ROOM, int *LOCK_CHANGING_ROOM);

int min_changing_room(int *NUMBER_CHANGING_ROOM);

#endif // CHANGING_ROOM
