#ifndef SIGNAL
#define SIGNAL

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>

#include "logger.h"
#include "def.h"

int stop;
int id;
void signal_handler(int _sig_num);
void set_signal_hadling(int _id);
int stopRunning();

#endif //SIGNAL
