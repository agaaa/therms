# Therms (Distribiuted programming final project) #

## PROBLEM ##
Therms contains 3 changing rooms with 2 cabinets (variable M defined in def.h), 3 showers (variable P defined in def.h) and one swimming pool.
Each client (woman or man) goes to the changing room (occupies one cabinet), takes a shower, goes to the swimming pool, comes back to the same changing room, releases cabinet and lefts the therms. What is more:
- women and men can't share the same chaniging room
- changing room can't be assigned to the gender (when it is empty, anyone can get in)
- clients come back to the therms after random time period
- clients spend random time in changing room, taking shower and in the pool
- client's gender is assigned randomly
- the number of clients in the swimming pool is unlimited

## REQUIREMENTS ##
mpi

## BUILDING ##
make

## RUNNING ##
mpirun -np *number_of_clients_processes* ./bin/main
